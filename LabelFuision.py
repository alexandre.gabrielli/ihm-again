class LabelFusion:
    def __init__(self, label, text, box, delete, color, layout):
        self.label = label
        self.text = text
        self.box = box
        self.delete = delete
        self.color = color
        self.layout = layout

        text.bind(text=self.setlabelname)

    def getcolor(self):
        return self.color

    def getlabel(self):
        return self.label.tojson()

    def getdelete(self):
        return self.delete

    def setlabelname(self, i_dont_know, name):
        self.label.rename(name)

    def setlabelrectangle(self, x, y, width, height):
        self.label.changerectangle(x, y, width, height)

    def ischecked(self):
        return self.box.active


    def getlayout(self):
        return self.layout