import json

from kivy.graphics.context_instructions import Color



class MyLabel:
    """Un label contient un nom, une position (x, y) de départ, une largeur et une hauteur"""

    def __init__(self, name):
        self.name = name
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0

    def changerectangle(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def rename(self, name):
        self.name = name

    def getname(self):
        return self.name

    def tojson(self):
        message = {
            "name": self.name,
            "coord": {
                "p1": "(" + str(self.x) + "," + str(self.y) + ")",
                "p2": "(" + str(self.x + self.width) + "," + str(self.y) + ")",
                "p3": "(" + str(self.x) + "," + str(self.y + self.height) + ")",
                "p4": "(" + str(self.x + self.width) + "," + str(self.y + self.height) + ")"
            }
        }
        return message