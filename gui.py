import datetime
import errno
import json
import os

import kivy
from kivy.uix.checkbox import CheckBox
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

from LabelFuision import LabelFusion
from MyLabel import MyLabel

kivy.require("1.10.1")
from kivy.app import App
from fileSelector import MyWidget, selected
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.filechooser import FileChooserIconView
from kivy.graphics import Rectangle, Color


labels = []

class MenuBar(BoxLayout):
    pass


class imageSelector(BoxLayout):
    def __init__(self, **kwargs):
        super(imageSelector, self).__init__(**kwargs)
        self.master = None
        container = BoxLayout(orientation='vertical')
        filechooser = FileChooserIconView()
        filechooser.bind(on_selection=lambda x: selected(filechooser.selection))
        open_btn = Button(text='open', size_hint=(1, .2))
        open_btn.bind(on_release=lambda x: {
            self.master.changetext(filechooser.selection, self.master._file)
        })
        container.add_widget(filechooser)
        container.add_widget(open_btn)
        self.add_widget(container)

    def newparent(self, parent):
        print(parent)
        self.master = parent


class EmplacementSelector(BoxLayout):
    def __init__(self, **kwargs):
        super(EmplacementSelector, self).__init__(**kwargs)
        self.master = None
        container = BoxLayout(orientation='vertical')
        filechooser = FileChooserIconView()
        filechooser.bind(on_selection=lambda x: selected(filechooser.selection))
        open_btn = Button(text='open', size_hint=(1, .2))
        open_btn.bind(on_release=lambda x: {
            self.master.changetext(filechooser.selection, self.master._emplacement)
        })
        container.add_widget(filechooser)
        container.add_widget(open_btn)
        self.add_widget(container)

    def newparent(self, parent):
        print(parent)
        self.master = parent


class FileSelector(App):
    def build(self):
        return MyWidget()


class MainPanel(BoxLayout):
    pass


class Test(BoxLayout):
    def __init__(self, **kwargs):
        # Always call super() when overriding a function of the super class
        super().__init__(**kwargs)
        self.parentRectangle = None
        self.size = (100, 100)
        self.r = None
        self.b = None
        self.g = None
        self.posx = None
        self.posx1 = 0.0
        self.posx2 = 0.0
        self.bool = False
        self.rectanglelist = []
        self.colorRBG = []
        with self.canvas:
            Color(1, 1, 0, 0.0, mode='rgba')
            Rectangle(pos=self.pos, size=self.size)

    def on_touch_down(self, touch):
        self.posx = (touch.x, touch.y)
        self.posx1 = touch.x
        self.posx2 = touch.y
    def setBool(self):
        self.bool = True
    def remove(self,i):
        print("I remove")
        print(i)
        self.rectanglelist.remove(self.rectanglelist[i])
        self.colorRBG.remove(self.colorRBG[i])
        w = 0
        self.canvas.clear()
        while w < len(self.rectanglelist):
            print(self.colorRBG[w])
            print(self.rectanglelist[w])
            self.canvas.add(self.colorRBG[w])
            self.canvas.add(self.rectanglelist[w])
            w += 1

    def on_touch_up(self, touch):
        print (self.rectanglelist)
        print (touch.x)
        print (touch.y)
        if self.bool and touch.x <600 and touch.y <530 and len(self.rectanglelist) > 0:
            test = touch.x - self.posx1
            test2 = touch.y - self.posx2
            self.rectanglelist[len(self.rectanglelist) - 1] = Rectangle(pos=self.posx, size=(test, test2))
            labels[len(labels)-1].changerectangle(self.posx1,self.posx2, touch.x ,touch.y)
            i = 0
            self.canvas.clear()
            while i < len(self.rectanglelist):
                self.canvas.add(self.colorRBG[i])
                self.canvas.add(self.rectanglelist[i])
                i += 1
    def defineParentRectangle(self, rectangle):
        self.rectanglelist.append(rectangle)
        print("add rectangle")

    def defineRGB(self, r, g, b):
        self.colorRBG.append(Color(r, g, b, 0.4, mode='rgba'))


class GUI(FloatLayout):
    def __init__(self, **kwargs):
        # Always call super() when overriding a function of the super class
        super().__init__(**kwargs)
        print(self.ids)
        self.popupWindow = None
        self.curentRectangle = None

        # Step 1: Retrieve your widgets (here pmw1 i.e. PlusMinusWidget 1)
        # self._pmw1 = self.ids.pmw1
        # self._lbl1 = self.ids.lbl1
        self.label_list = []

        self._mb = self.ids.mb

        self._file = self._mb.ids.file
        self._selectImage = self._mb.ids.selectImage
        self._emplacement = self._mb.ids.emplacement
        self._selectEmplacement = self._mb.ids.selectEmplacement
        self._newLabel = self._mb.ids.newLabel

        self._save = self.ids.save
        self._img = self.ids.img
        self._grid = self.ids.grid

        # You can chain the call of `ids` to retrieve nested widget
        # like here with `btn_plus`
        #  btn_plus_w1 = self._pmw1.ids.btn_plus
        # btn_plus_w1.text = "Add 1"
        self._file.bind(text=self.on_text)
        self._selectImage.bind(on_press=self.callbackselectimage)
        self._selectEmplacement.bind(on_press=self.callbackselectEmplacement)
        self._newLabel.bind(on_press=self.addlabel)
        self._save.bind(on_press=self.save)
        self.layoutCanvas = Test()
        self.add_widget(self.layoutCanvas)


    # Step 2: add the callback function, do not add () at the end of the function

    def on_text(self, position, value):
        self._img.source = value
        self._img.reload()
        # self.canvas.texture=self._img

    def changetext(self, val, window):
        window.select_all()
        window.delete_selection()
        for elem in val:
            window.text += elem
        self.popupWindow.dismiss()

    def callbackselectimage(self, value):
        print('My button <%s> state is <%s>' % (self, value))
        show = imageSelector()
        show.newparent(self)
        self.popupWindow = Popup(title="select you're Image", content=show, size_hint=(None, None), size=(400, 400))
        self.popupWindow.open()

    def callbackselectEmplacement(self, value):
        print("selected emplacement")
        popup = EmplacementSelector()
        popup.newparent(self)
        self.popupWindow = Popup(title="select the folder you wanna register you're work", content=popup,
                                 size_hint=(None, None), size=(400, 400))
        self.popupWindow.open()

    def addlabel(self, value):
        mylabel = MyLabel(name="monLabel")
        labels.append(mylabel)
        import random
        r = random.uniform(0, 1)
        g = random.uniform(0, 1)
        b = random.uniform(0, 1)
        mycolor = (r, g, b, random.uniform(0.3, 1))
        box_layout = BoxLayout()
        checkbox = CheckBox()
        checkbox.group = "Label"
        box_layout.add_widget(checkbox)
        textinput = TextInput(hint_text=mylabel.getname())
        textinput.background_color = mycolor
        textinput.size = (150, 30)
        textinput.size_hint = (None, None)
        box_layout.add_widget(textinput)
        box_layout.size = (200, 30)
        box_layout.size_hint = (None, None)
        delete = Button(text="x", background_color=(1.0, 0.0, 0.0, 1.0), size=(30, 30))

        self.layoutCanvas.defineParentRectangle(Rectangle())
        self.layoutCanvas.defineRGB(r, g, b)
        self.layoutCanvas.setBool()
        # test.bind(on_press=self.canvasFonction)
        box_layout.add_widget(delete)
        self.label_list.append(LabelFusion(mylabel, textinput, checkbox, delete, mycolor, box_layout))
        delete.bind(on_press=self.deletelabel)
        self._grid.add_widget(box_layout)

    def deletelabel(self, value):
        removelater = 0
        i = 0
        for labelfusion in self.label_list:
            if labelfusion.getdelete() == value:
                self._grid.remove_widget(labelfusion.getlayout())
                removelater = labelfusion
                break
            i+=1
        self.layoutCanvas.remove(i)
        self.label_list.remove(removelater)

    def renamelabel(self, value):
        for labelfusion in self.label_list:
            if labelfusion.ischecked():
                labelfusion.setlabelname(value)
                break

    def save(self, value):
        if not os.path.exists(os.path.dirname(self._emplacement.text)):
            try:
                os.makedirs(os.path.dirname(self._emplacement.text))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        f = open(self._emplacement.text + "/output-" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                 + ".json", "w+")
        jsonlist = []
        for labelfusion in self.label_list:
            jsonlist.append(labelfusion.getlabel())
        contenu = {
            "image": self._file.text,
            "labels": jsonlist
        }
        f.write(json.dumps(contenu))
        f.close
        layout = GridLayout(cols=1, padding=10)
        popupLabel = Label(text="The output was generated.")
        closeButton = Button(text="Close")
        layout.add_widget(popupLabel)
        layout.add_widget(closeButton)
        popup = Popup(title='Success',
                      content=layout, size_hint =(None, None), size =(200, 200))
        popup.open()
        closeButton.bind(on_press=popup.dismiss)


class GUIApp(App):
    def build(self):
        return GUI()


if __name__ == "__main__":
    GUIApp().run()
