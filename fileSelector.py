from kivy.app import App
from kivy.uix.boxlayout import BoxLayout

from kivy.uix.button import Button
from kivy.uix.filechooser import FileChooserIconView

file = ""


def selected(filename):
    file = filename[0]


def getfile():
    return file


class MyWidget(BoxLayout):

    def __init__(self, **kwargs):
        super(MyWidget, self).__init__(**kwargs)

        container = BoxLayout(orientation='vertical')

        filechooser = FileChooserIconView()
        filechooser.bind(on_selection=lambda x: selected(filechooser.selection))

        open_btn = Button(text='open', size_hint=(1, .2))
        open_btn.bind(on_release=lambda x: {
            print(filechooser.selection)
        })
        container.add_widget(filechooser)
        container.add_widget(open_btn)
        self.add_widget(container)


class FileSelector(App):
    def build(self):
        return MyWidget()


if __name__ == '__main__':
    FileSelector().run()
