import tkinter as tk
from PIL import Image, ImageTk
import os


# --- functions ---

def on_click(event=None):
    # `command=` calls function without argument
    # `bind` calls function with one argument
    print("image clicked")

# --- main ---

# init
root = tk.Tk()

# load image
image = Image.open(os.path.abspath('folder.png'))
photo = ImageTk.PhotoImage(image)

# label with image
l = tk.Label(root, image=photo)
l.grid(row=0,column=0)

# bind click event to image
l.bind('<Button-1>', on_click)

# button with image binded to the same function
b = tk.Button(root, image=photo, command=on_click)
b.grid(row=1,column=0)

# button with text closing window
b = tk.Button(root, text="Close", command=root.destroy)
b.grid(row=2,column=0)

# "start the engine"
root.mainloop()